import React from 'react'
import MaterialTable from 'material-table';

export default class BasicTreeData extends React.Component {
    state = {
        columns: [
            { title: 'Nome', field: 'name' },
            { title: 'Tipo', field: 'type' },
            { title: 'Descrição', field: 'description' },
            { title: 'Obrigatoriedade', field: 'Mandatory' }
        ],
        message: null
    }

    render() {
        const parametrosEntrada = this.props.item
        return (
            <MaterialTable 
                title="Parametros de Entrada"
                data={parametrosEntrada}
                columns={this.state.columns}

                options={{
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    rowStyle: {
                        backgroundColor: '#f2f2f2',
                    },
                    headerStyle: {
                        backgroundColor: '#dcdcdc',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    }
                }}
                parentChildData={(row, rows) => rows.find(a => a.name === row.parentName)}
            />
        )
    }
}