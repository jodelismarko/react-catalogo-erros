import React from 'react'
import MaterialTable from 'material-table';

export default class BasicTreeData extends React.Component {
    state = {
        columns: [
            { title: 'Codigo', field: 'codigo' },
            { title: 'Mensagem', field: 'mensagem' },
        ],
        message: null
    }

    render() {
        console.log(this.props.item)
        const erros = this.props.item
        return (
            <MaterialTable 
                title="Erros"
                data={erros}
                columns={this.state.columns}
             
                options={{
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    rowStyle: {
                        backgroundColor: '#f2f2f2',
                    },
                    headerStyle: {
                        backgroundColor: '#dcdcdc',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    }
                }}
            />
        )
    }
}