import React from 'react'
import './styles.css'
import MaterialTable from 'material-table';

export default class BasicTreeData extends React.Component {
    state = {
        columns: [
            { title: 'Nome', field: 'name' },
            { title: 'Tipo', field: 'type' },
            { title: 'Descrição', field: 'description' }
        ],
        message: null
    }

    render() {
        console.log(this.props.item)
        const parametrosSaida = this.props.item
        return (
            <MaterialTable 
                title="Parametros de Saída"
                data={parametrosSaida}
                columns={this.state.columns}

                options={{
                    rowStyle: rowData => ({
                        backgroundColor: (!rowData.parentName) ? '#f2f2f2' : '#FFF'
                    }),
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    headerStyle: {
                        backgroundColor: '#dcdcdc',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    }
                }}
                parentChildData={(row, rows) => rows.find(a => a.name === row.parentName)}

            />
        )
    }
}