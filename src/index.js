import React from 'react';
import App from './Main';
import View from './Servico'
import './css/index.css';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route } from 'react-router-dom';
import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';

ReactDOM.render(
  <BrowserRouter>
    <React.Fragment>
      <Route exact path="/" component={App} />
      <Route path="/servicos/:id" component={View} />
    </React.Fragment>
  </BrowserRouter>,
  document.getElementById('root')
);


serviceWorker.unregister();
