import React from 'react';
import ListaServicos from '../Servicos/ListServicosComponent'
import './styles.css';


function App() {
  return (
    <>
      <h1 style={{textAlign:"center"}}>Catalogo de Serviços e Erros do Outsystems</h1>
      <ListaServicos/>
    </>
  )
}

export default App;