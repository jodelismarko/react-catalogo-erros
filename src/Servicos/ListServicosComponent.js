import ServicosDataService from '../Service/ServicosDataService';
import createHistory from 'history/createBrowserHistory';
import MaterialTable from 'material-table';
import React, { Component } from 'react'

export const history = createHistory();

class ListServicosComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      courses: [],
      columns: [
        { title: 'Id', field: 'id' },
        { title: 'Nome do Método', field: 'metodoname', initialEditValue: 'Valor Inicial' },
        { title: 'Descrição', field: 'description' },
      ],
      message: null
    }
    this.viewServicoClicked = this.viewServicoClicked.bind(this)
  }

  componentDidMount() {
    this.refreshServicos();
  }

  async refreshServicos() {
    const response = await ServicosDataService.retrieveAllServicos();
    this.setState({ courses: response.data })
  }

  viewServicoClicked(id) {
    history.push(`/servicos/${id}`)
    window.location.reload(false);
  }

  async addServicoClicked() {
  }
  async updateServicoClicked() {
  }
  async deleteCourseClicked() {
  }

  render() {
    return (
      <div>
        {this.state.message && <div class="alert alert-success alert-dismissible fade show" role="alert" >{this.state.message}
          <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true" >&times;</span>
          </button>
        </div>}
        <MaterialTable
          title="Todos os Serviços"
          columns={this.state.columns}
          data={this.state.courses}
          actions={[
            {
              icon: "add",
              tooltip: 'Adicionar Serviço',
              isFreeAction: true,
              onClick: (event) => this.addServicoClicked(),
            },
            {
              icon: 'visibility',
              tooltip: 'Virualizar Servico',
              onClick: (event, rowData) => this.viewServicoClicked(rowData.id),
            },
            {
              icon: 'edit',
              tooltip: 'Editar Servico',
              onClick: (event, rowData) => this.viewServicoClicked(rowData.id),
            },
            {
              icon: 'delete',
              tooltip: 'Delete Servico',
              onClick: (event, rowData) => { if (window.confirm('Você tem certeza que quer deletar este Item')) this.deleteCourseClicked(rowData.id) }
            }
          ]}

          options={{
            actionsColumnIndex: -1,
            pageSize: 10,
            search: true
          }}
        />
      </div>
    )
  }
}

export default ListServicosComponent
