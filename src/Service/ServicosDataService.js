// import axios from "axios";

// const Service = axios.create({baseURL: 'http://localhost:4200'});

// export default Service;

import axios from 'axios'

const COURSE_API_URL = 'http://localhost:4200'

class ServicosDataService {

    retrieveAllServicos() {
        //console.log('executed service')
        return axios.get(`${COURSE_API_URL}/servicos`);
    }

    async retrieveServico(id) {
        //console.log('executed service')
        const response = await axios.get(`${COURSE_API_URL}/servicos/${id}`);
        return response;
    }

    updateServico(id, servico) {
        //console.log('executed service')
        return axios.put(`${COURSE_API_URL}/servicos/${id}`, servico);
    }
    deleteServico(id) {
        //console.log('executed service')
        return axios.delete(`${COURSE_API_URL}/servicos/${id}`);
    }
    criarServico(servico) {
        return axios.post(`${COURSE_API_URL}/servicos/`, servico);
    }
   
}

export default new ServicosDataService()
